/*jslint node: true, sloppy: true, white: true*/
/**
 * NoteController
 *
 * @description :: Server-side logic for managing notes
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {


  index: function (req, res) {
    Note.create({
      text: "text"
    });
  },

  /**
   * `NoteController.text()`
   */
  text: function (req, res) {
    return res.json({
      todo: 'text() is not implemented yet!'
    });
  },


  /**
   * `NoteController.tags()`
   */
  tags: function (req, res) {
    return res.json({
      todo: 'tags() is not implemented yet!'
    });
  },
  
  
  
  
  //Real functions
  
  //POST /notes
  createNote: function (req, res){
    var userId = req.session.passport.user;
    Note.create().then(function(note){
      note.users.add(userId);
      note.save(console.log);
      res.json(note);
    });
  },
  
  
  

  getTags: function (req, res){
    var noteid = req.params.noteid;
    //Do something if no note is found (can't populate then)
    Note.findOne(noteid).populate('tags').then(function(note){
      res.json(note.tags);
    });
  }


};


/**
 * TagController
 *
 * @description :: Server-side logic for managing tags
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `TagController.text()`
   */
  text: function (req, res) {
    return res.json({
      todo: 'text() is not implemented yet!'
    });
  },


  /**
   * `TagController.notes()`
   */
  notes: function (req, res) {
    return res.json({
      todo: 'notes() is not implemented yet!'
    });
  },
  
  getNotes: function (req, res){
    var tagid = req.params.tagid;
    Tag.findOne(tagid).populate('notes').then(function(tag){
      res.json(tag.notes);
    });
  }
};


/*jslint node: true, sloppy: true, white: true*/
/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
  
  getNotes: function (req, res){
    var userid = req.params.userid;
    User.findOne(userid).populate('notes').then(function(user){
      res.json(user.notes);
    });
    
    
    
    
    
    //Do something if no note is found (can't populate then)
//    Note.findOne(userid).populate('tags').then(function(note){
//      res.json(note.tags);
//    });
  }
	
};


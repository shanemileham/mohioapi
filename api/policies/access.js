/*jslint node: true, sloppy: true, white: true*/

// policies/access.js
module.exports = function canWrite (req, res, next) {

  //Okay if GET
  var method = req.method;
  console.log('Method: ' + req.method);
  if (method === 'GET'){
    next();
  }else{

    //Check if admin
    var userId = req.session.passport.user;
    console.log('User ID: ' + userId);
    if (userId < 3){
      next();
    }

  }

  //  var targetId = req.param('id');
  //  console.log('Target ID: ' + targetId);
  //  
  //  
  //  if (userId < 3 || userId === targetId || method === 'GET'){
  //    next();
  //  }
};